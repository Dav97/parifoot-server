
"use strict";
var fs = require('fs');

exports.tokenSignOptions = {
    issuer: 'GISBenin',
    subject: 'seidoudavid97@gmail.com',
    audience: 'https://www.ifri-uac.bj',
    algorithm: "RS256",
    expiresIn: 36000
};
exports.tokenVerifyOptions = {
    issuer: 'GISBenin',
    subject: 'seidoudavid97@gmail.com',
    audience: 'https://www.ifri-uac.bj',
    algorithm: "RS256"
};

exports.message = {
    authFailError: { error: "Either the password or the email are valid" }
};

exports.ROLES = {
    USER_ROLE: "user",
    MANAGER_ROLE: "manager",
    ADMIN_ROLE: "admin"
};
//exports.sendgridToken = process.env.MAIL_TOKEN;
//const sender = sendgrid.setApiKey(exports.sendgridToken);



exports.jwtSecret = process.env.JWT_SECRET;

exports.uploadsDir = {
    privateKEY: fs.readFileSync('./security/private.key', 'utf8'),
    publicKEY : fs.readFileSync('./security/public.key', 'utf8')
};


// configuration autho Service

// create request
