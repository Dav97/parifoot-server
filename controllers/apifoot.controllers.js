var axios = require('axios');

var getLeague = function(req, res){
    axios.get(`https://apifootball.com/api/?action=get_leagues&country_id=${req.params.country_id}&APIkey=${process.env.API_FOOTBALL_KEY}`).then((response) => {
        res.status(200).send({
            sucess: 'true',
            message: 'find event today',
            EventList: response.data
        })
    }).catch((err)=>{
        res.status(400).send({
            sucess:"false",
            message: "Cannot find match",
            Eror: err
        })
    })
}

var getCountry = function(req, res){
    axios.get(`https://apifootball.com/api/?action=get_countries&APIkey=${process.env.API_FOOTBALL_KEY}`).then((response) => {
        res.status(200).send({
            sucess: 'true',
            message: 'find country today',
            EventList: response.data
        })
    }).catch((err)=>{
        res.status(400).send({
            sucess:"false",
            message: "Cannot find country",
            Eror: err
        })
    })
}

module.exports = {
   getLeague: getLeague,
   getCountry: getCountry
}