var request = require("request");

var options = {
  method: 'PUT',
  url: 'https://YOUR_DOMAIN/api/v2/guardian/factors/email',
  headers: {
    authorization: 'Bearer MANAGEMENT_API_TOKEN',
    'content-type': 'application/json'
  },
  body: {enabled: true},
  json: true
};

request(options, function (error, response, body) {
  if (error) throw new Error(error);

  console.log(body);
});

