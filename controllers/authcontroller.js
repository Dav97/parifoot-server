var express = require('express');
var fs = require('fs');
var router = express.Router();
var bodyParser = require('body-parser');
var model = require('../models/user.model').userModel;
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var config = require('../config');
var auth = require('../helpers/auth');
require('../db')
var request = require('request')


router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());


/* ###### SIGN UP TO APP ########*/

router.post('/register', function(req, res) {
  var hashedPassword = bcrypt.hashSync(req.body.password, 8);
  if (!req.body.phoneNumber) {
    return res.status(400).send({
      success: 'false',
      message: 'phone number is required',
    });
  } else if (!req.body.password) {
    return res.status(400).send({
      success: 'false',
      message: 'lecture is required',
    });
  } else if(!req.body.pseudo){
    return res.status(400).send({
      success:'false',
      message: 'pseudo not find'
    })
  }
 
  const user = {
    pseudo : req.body.pseudo,
    password: hashedPassword,
    email: req.body.email,
    phoneNumber: req.body.phoneNumber,
    lastname: req.body.lastname,
    firstname: req.body.firstname
  };

  /* use Token */
  var payload = {
    email : user.email,
    pseudo: user.pseudo
  }
  var token = jwt.sign(
    payload, // payload //
    config.uploadsDir.privateKEY, // private key for  security authenticate //
    config.tokenSignOptions // options de signature //
  )
  model.create(user, function(err){
      var error = err;
      return res.status(201).send({
        success: 'true',
        message: 'user added successfully',
        user: user,
        token: token
      });
  })
});


/* ###### SIGN IN TO APP ########*/

router.post('/login', function(req, res){
  const body = {
    email: req.body.email,
    password: req.body.password    
  }
  if(body.email === undefined || body.password === undefined){
    auth.authFailed(res);
    return;
  }
  if(body){
    // get user in database //
    model.findOne({email: body.email}, function(err, user){
      if(err){
        return res.status(500).send({
          sucess: 'false',
          message: 'not found user',
          eror: err
        })
      }
      else{
        if(user === undefined || user === null ){
          return res.status((401).send({
            sucess: 'false',
            message: ' not correct user'
          }))
        }
        else{
          const hash = user.email;
          t = bcrypt.compareSync(body.password, hash);
          if(!t)
          {
             return res.status(200).send({
               sucess: 'true',
               message: ' loging sucess',
               token:  jwt.sign((config.tokenSignOptions), config.uploadsDir.privateKEY)
             });
          }

        }
      }
    })
  }
})

/* ###### User LOGOUT TO APP ########*/

router.post('/logout', function(req, res){
  const token = req.headers['x-access-token'];
  
    return res.status(400).send({
      sucess: 'true',
      message: 'logout sucessful to app',
      token: null
    })
  
})

/* ###### GET LIST OF USER TO APP ########*/
router.get('/list', function(req, res){
  var userList;  
  model.find(null, function(err, data){
    if(!err){
       return res.status(200).send({
        sucess: 'true',
        message: 'Retrieve list of users sucessful',
        data: data
      })
    }else{
    console.log(err);
    }
  });  
 })

 router.get('/auth0', function(req, res){
  var options = {
    method: 'POST',
    url: 'https://dev-uirh01pn.auth0.com/oauth/token',
    headers: {'content-type': 'application/x-www-form-urlencoded'},
    form: {
      grant_type: 'client_credentials',
      client_id: 'Ec02FMAzmA2yywKdS0NeFb02SFBSrqMD',
      client_secret: process.env.AUTH0_CLIENT_ID,
      audience: 'https://dev-uirh01pn.auth0.com/api/v2/'
    }
  };

var data ;
request(options, function (error, response, body) {
  if (error) throw new Error(error);
  data = body
  console.log(data);
});

  if(data) {
    return res.status(200).send({
      sucess: 'true',
      message: 'Retrieve list of users sucessful',
      data: data
    })
  }
  
 })

// Authentification with Auth0 




module.exports = router;
