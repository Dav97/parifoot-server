var axios = require('axios')
var model = require('../models/user.model')
var UserModel = model.userModel;
var router = require('express').Router()


    /* Make request payment */
    RequestPayment = (req, res) => {
        let body = req.body;
        body.prototype.clientid = "10254"
        if(body === null){
            res.Status(400).send({
                sucess: 'false',
                message: 'request is not valid'
            })
        }else{
            UserModel.findOneAndUpdate({phoneNumber: body.msisdn}, 
                                       { lastname: body.lastname},
                                       { firstname: body.firstname},
                                       { amount: body.amount},
                                       {transref: body.transref}
            )
            .select()
            .exec(
                (err, user) => {
                    if(err){
                        res.status(404).send({
                            sucess: "false",
                            message: `${body.msisdn} is not found, please do a sign up`
                        })
                    }
                    else{
                        if(body.amount){
                            console.log("solde old =" + user.amount);
                            user.amount += body.amount;
                            console.log("solde current =" + user.amount + "solde add now" + body.amount);
                            res.status(200).send({
                                sucess:'true',
                                message: 'Send Solde add to amount Sucesful',
                                user: user
                            });
                            if(body.firstname !="" && body.lastname !=""){
                                body.msisdn = process.env.AMOUNT_PAYMENT.toString();
                                body.clienid = process.env.CLIENT_ID;
                                let headers = {
                                        Accept: 'application/json',
                                        'X-AuthToken': localStorage.getItem('token')
                                }
                                axios.post("https:QosicBridge/user/requestpayment", {body}).then(
                                    (response) => {
                                        const data  = response;
                                    res.status(200).send({
                                        sucess: "true",
                                        message: "Sucessful to request payment",
                                        data: data
                                    });
                                })
                            }else{
                                res.status(400).send({
                                    sucess: "false",
                                    message: "Eror to request payment invalid lastname, firstname"
                                });
                            }
                        }
                        
                    }}
            )
        }
    }

    /* Get Account Status */
    GetStatusAccount = (req, res) => {
        let body = {
            transref: req.body.transref,
            clientid: req.body.clientid
        }
        axios.post("https://QosicBridge/user/gettransactionstatus", body).then(
            (response) => {
                res.status(200).send({
                    sucess: "true",
                    message: 'get status account succesful',
                    data: response.data
                })
            }
        ).catch((err) => {
            res.status(400).send({
                sucess:' false',
                message: "bad request to get status account",
                error: err
            })
        })
    }

    /* Make deposit money */
    MakeDeposit = (req, res) =>{
        const request = {
            "msisdn" : "",
            "amount" : "",
            "transref" : "",
            "clientid" : ""
        }
        let body = {
            msisdn : req.body.msisdn,
            amount : req.body.amount,
            transref: req.body.transref,
            clientid: process.env.CLIENT_ID
        }
        if(body.msisdn != "" && body.amount != ""){
            UserModel.findone(null, {msisdn : body.msisdn}, (err, user) => {    
                if(err){
                    res.status(203).send({
                        sucess: "false",
                        message: "Subscribe before",
                        eror : err
                    })
                }
                else{
                    axios.post("https://QosicBridge/user/deposit", body).then( 
                        (response) => {
                            res.status(200).send({
                                sucess: "true",
                                message: "Sucesful Deposit money",
                                responseDeposit : response.data,
                                user: user
                            }) 
                        }
                    ).catch(
                        (err) => {
                            res.status(203).send({
                                sucess: "false",
                                message: "Eror bind to Deposit over Api Momo",
                                eror : err
                            })
                        }
                    )
                }
            })
        }
        else if(body.clientid === "" && body.transref === ""){
            res.status(203).send({
                sucess: "false",
                message: "Put to your request (clienid & transref)",
            })
        }
        else{
            res.status(203).send({
                sucess: "false",
                message: "Put to your request (amount & misisdn)",
            })
        }
    }



module.exports = router;