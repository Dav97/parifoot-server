var model = require('../models/user.model')
var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var model = require('../models/user.model').userModel;
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var config = require('../config');
require('../db')


router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

/* ###### GET LIST OF USER TO APP ########*/
router.get('/list', function(req, res){
    var userList;  
    model.find(null, function(err, data){
      if(!err){
         return res.status(200).send({
          sucess: 'true',
          message: 'Retrieve list of users sucessful',
          data: data
        })
      }else{
      console.log(err);
      }
    });  
   })
                  
   
   /* ######  GET USER TO APP ########*/
   router.get('/me', function(req, res) {
  
    var token = req.headers['x-access-token'];
    console.log(config.secret);
  
    if (!token) return res.status(401).send({ auth: false, message: 'No token provided.' });
    
    jwt.verify(token, config.uploadsDir.publicKEY, config.tokenVerifyOptions, function(err, decoded) {
      if (err) return res.status(500).send({ auth: false, message: 'Failed to authenticate token.' });
  
      model.findById(decoded.email, function (err, user) {
        if (err) return res.status(500).send({
          sucess: 'false',
          message: "There was a problem finding the user.",
          error: err
        });
        if (!user) return res.status(404).send({
          sucess: 'false',
          message: `user id ${decoded.email} not found`,
          decoded: decoded
        });
        res.status(200).send({
          succes: 'true',
          message: "get user succesful",
          decode: decoded.email
        });
      });     
  });
   })

/*######### CREATE USER ##########*/
router.post('/create', function(req, res) {
    var hashedPassword = bcrypt.hashSync(req.body.password, 8);
    if (!req.body.phoneNumber) {
      return res.status(400).send({
        success: 'false',
        message: 'phone number is required',
      });
    } else if (!req.body.password) {
      return res.status(400).send({
        success: 'false',
        message: 'lecture is required',
      });
    } else if(!req.body.pseudo){
      return res.status(400).send({
        success:'false',
        message: 'pseudo not find'
      })
    }
   
    const user = {
      pseudo : req.body.pseudo,
      password: hashedPassword,
      email: req.body.email,
      phoneNumber: req.body.phoneNumber,
      lastname: req.body.lastname,
      firstname: req.body.firstname
    };
  
    /* use Token */
    var payload = {
      email : user.email,
      pseudo: user.pseudo
    }
    var token = jwt.sign(
      payload, // payload //
      config.uploadsDir.privateKEY, // private key for  security authenticate //
      config.tokenSignOptions // options de signature //
    )
    model.create(user, function(err){
        var error = err;
        return res.status(201).send({
          success: 'true',
          message: 'user added successfully',
          user: user,
          token: token
        });
    })
  });

  /* ###### Delete some user ####### */

  router.delete('/delete', function(req, res){
    /* get user --*/
    var id = req.params.id;
    var word = req.params.word
    model.find(null, function(err, data){
      for(const i =0; i< data.length + 1; i++){
        //if()
      }
    })
  })

 



