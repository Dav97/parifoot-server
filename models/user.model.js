var mongoose = require('mongoose')
var schema = require('./user.schema');
var userSchema = schema.UserSchema;

userModel = mongoose.model('users', userSchema);

module.exports = {
    userModel : userModel
}